# git basico

## **Una mini guía de uso de Git con GitLab**

### 1. Sube algún script que hayas hecho en los ejemplos o desafíos anteriores al repositorio remoto.
-   Crear un proyecto haciendo click en el botón azul New Project (Nuevo Proyecto).
-   Crear un proyecto en blanco (Blank Project)
- Completar el campo de nombre, url que tendrá nuestro proyecto, generalmente usuario o grupo seguido de un slug (apodo para el proyecto que sea breve y conciso) y la visibilidad como Privada.

**- ¡Listo! Ahora debemos crear un respositorio para nuestro proyecto.**

- Ahora clonamos el repositorio de GitLab en nuestra computadora local.
  En el terminal escribimos:

    `$ git clone https://gitlab.com/tuusuario/tuproyecto.git`

- Nos movemos a la carpeta clonada:

    `cd carpeta-clonada/`

- Creamos un nuevo documento dentro de nuestra nueva carpeta.

    `echo "print('Serpiente')" >> miscript.py`

- Agregamos el archivo a _staging_ (que es un estado intermedio en el que se van almacenando los archivos antes de commit)con:

    `git add miscript.py`

- Preparamos el archivo para subirlo con (con commit lo enviamos al repositorio local. -m para agregar un mensaje al commit):

    `git commit -m "añadir miscript"`

- Lo subimos al repositorio remoto (_`-u` para establecer un enlace entre ellos especificando también el repositorio remoto (origin) y la rama de trabajo (master) )_ con el comando:

    `git push -u origin master`
 
**¡Listo! Subiste tu primer archivo de código al repositorio online.**

### 2. Intenta modificar ese código, agregando o sacando líneas en tu versión local. Luego vuélvelo a subir haciendo un push.

- Modificamos el archivo miscript.py y hacemos un commit de los cambios _(-am Si solo has cambiado archivos existentes y no has creado otros nuevos)_:

    `echo "print('Serpientes\nSnakes')" > miscript.py`

    `git commit -am "Nueva version"`

- Si vemos el status ahora, este nos indica que hay cambios pendientes de sincronizar con el remoto:

    `git status`

- Solicitamos los ultimos cambios en repo mediante `pull` _(les sale un mensaje para explicar por qué es necesaria esta fusión)_, y lo subimos al repositorio remoto:

    `git pull origin master`

    `git push origin master`
    
**Cool!**
